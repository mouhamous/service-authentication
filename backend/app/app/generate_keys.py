import os
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa

# Get the absolute path of the "app" directory
app_directory = os.path.dirname(os.path.abspath(__file__))

# generate private and public key
private_key = rsa.generate_private_key(public_exponent=65537, key_size=4096)

private_key_pem = private_key.private_bytes(
    encoding=serialization.Encoding.PEM,
    format=serialization.PrivateFormat.PKCS8,
    encryption_algorithm=serialization.NoEncryption(),
)

public_key = private_key.public_key()
public_key_pem = public_key.public_bytes(
    encoding=serialization.Encoding.PEM,
    format=serialization.PublicFormat.SubjectPublicKeyInfo,
)

# saving the keys
if not os.path.exists(f"{app_directory}/keys"):
    os.makedirs(f"{app_directory}/keys")

with open(f"{app_directory}/keys/private_key.pem", "wb") as f:
    f.write(private_key_pem)

with open(f"{app_directory}/keys/public_key.pem", "wb") as f:
    f.write(public_key_pem)

print(f"Private key saved to {app_directory}/keys/private_key.pem")
print(f"Public key saved to {app_directory}/keys/public_key.pem")
