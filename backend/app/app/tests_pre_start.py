from app.db.session import SessionLocal
from app.core.config import settings


def init() -> None:
    try:
        db = SessionLocal()
        db.execute(f"DROP DATABASE {settings.db}")
        db.execute(f"CREATE DATABASE {settings.db}")
    except Exception as e:
        raise e


def main() -> None:
    init()


if __name__ == "__main__":
    main()
