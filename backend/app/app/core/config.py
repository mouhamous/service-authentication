import os
import secrets
from typing import Any, Dict, List, Optional, Union

from pydantic import AnyHttpUrl, BaseSettings, EmailStr, HttpUrl, AnyUrl, validator


def read_key(key_type: str):
    if key_type not in ["private", "public"]:
        raise ValueError("Invalid key type")
    key_file = f"./app/keys/{key_type}_key.pem"
    if not os.path.exists(key_file):
        raise FileNotFoundError(f"{key_file} not found")
    with open(key_file, "rb") as f:
        key_data = f.read()
    return key_data


class Settings(BaseSettings):
    API_V1_STR: str = "/api/v1"
    PRIVATE_KEY_PEM: str = read_key("private")
    PUBLIC_KEY_PEM: str = read_key("public")
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 60
    BACKEND_CORS_ORIGINS: List[AnyHttpUrl] = []
    user = os.getenv("MYSQL_USER")
    password = os.getenv("MYSQL_PASSWORD")
    server = os.getenv("MYSQL_SERVER", "db")
    db = os.getenv("MYSQL_DATABASE")
    SQLALCHEMY_DATABASE_URI = f"mysql+pymysql://{user}:{password}@{server}/{db}"
    FIRST_SUPERUSER: EmailStr
    FIRST_SUPERUSER_PASSWORD: str
    EMAIL_TEST_USER: EmailStr = "test@example.com"


settings = Settings()
